
function checkForValidUrl(tabId, changeInfo, tab) {
  if (tab.url.indexOf('http://www.btvsports.life') == 0) {
    chrome.pageAction.show(tabId);
  }
};

chrome.tabs.onUpdated.addListener(checkForValidUrl);

chrome.browserAction.onClicked.addListener(function(tab) {
  chrome.tabs.executeScript({
        file: 'jquery-3.2.1.min.js'
    }, function() {
        // Guaranteed to execute only after the previous script returns
        chrome.tabs.executeScript({
            file: 'clear.js'
        });
    });
 });
